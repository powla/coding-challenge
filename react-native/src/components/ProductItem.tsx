import React from "react";
import { Inventory } from "../store/inventory";
import { Card, Chip, List } from "react-native-paper";
import { Image, StyleSheet, View } from "react-native";

export default (props: { item: Inventory }) => {
  const [expanded, setExpanded] = React.useState(true);
  const {
    item: {
      fields: {
        Posted: posted,
        "Product Name": productName,
        "Product Image": productImage,
        "Product Categories": productCategories
      }
    }
  } = props;
  const categories = productCategories ? productCategories.split(", ") : [];

  const handlePress = () => setExpanded(!expanded);

  const isWithinLastSevenDays = (date: Date): boolean =>
    date > new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);

  return (
    <Card mode="elevated" elevation={1} style={styles.container}>
      <Card.Content style={styles.cardContent}>
        <List.Accordion
          style={[styles.container, styles.accordion]}
          rippleColor="#F8F9FC"
          title={productName}
          titleStyle={styles.productTitle}
          descriptionStyle={styles.productDate}
          description={new Date(posted).toLocaleDateString("de-DE", {
            year: "numeric",
            month: "2-digit",
            day: "2-digit"
          })}
          left={() => (
            <Image
              style={styles.productImage}
              source={
                productImage
                  ? { uri: productImage }
                  : require("../assets/images/no-image.png")
              }
            />
          )}
          expanded={expanded}
          onPress={handlePress}
        >
          {isWithinLastSevenDays(new Date(posted)) && (
            <View style={styles.newProductNotice}>
              <Image source={require("../assets/images/new-product.png")} />
            </View>
          )}
          {categories.length > 0 && (
            <View style={styles.chipContainer}>
              {categories.map((category, index) => (
                <Chip
                  key={index}
                  style={styles.chip}
                  textStyle={styles.chipText}
                >
                  {category}
                </Chip>
              ))}
            </View>
          )}
        </List.Accordion>
      </Card.Content>
    </Card>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#F8F9FC"
  },
  cardContent: {
    minHeight: 144,
    paddingVertical: 8,
    paddingHorizontal: 8
  },
  accordion: {
    paddingRight: 0,
    paddingVertical: 0
  },
  productImage: {
    width: 84,
    height: 112,
    resizeMode: "center"
  },
  productTitle: {
    fontSize: 20,
    fontWeight: "900",
    color: "black",
    position: "absolute",
    top: 0,
    left: 20,
    paddingRight: 53
  },
  productDate: {
    position: "absolute",
    top: 24,
    left: 20
  },
  newProductNotice: {
    paddingLeft: 12,
    paddingRight: 12,
    paddingVertical: 8,
    borderRadius: 9,
    borderTopRightRadius: 0,
    backgroundColor: "#333",
    position: "absolute",
    top: 8,
    right: 30
  },
  chipContainer: {
    flexDirection: "row",
    flex: 1,
    flexWrap: "wrap",
    gap: 6,
    marginTop: -60,
    marginLeft: 104,
    paddingLeft: 0
  },
  chip: {
    backgroundColor: "#D4E5FF",
    borderRadius: 48,
    height: 26
  },
  chipText: {
    fontSize: 12,
    marginVertical: 2,
    marginHorizontal: 12
  }
});
